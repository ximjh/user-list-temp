# User List Temp



## OS Version
macOS Ventura 13.3.1 (a)(22E772610a)

## Xcode Version
Version 14.3 (14E222b)

## Develope
iOS 13
<br/>
Storyboard
<br/>
Swift
<br/>
using RANDOM USER GENERATOR (https://randomuser.me/)

## Used Pod
Alamofire 5.4.4
<br/>
SwiftyJSON


## 조건
- 하나의 성별만으로 구성된 리스트가 노출 되어야 합니다.
    - segment 로 여성, 남성 구분
- 이름 표출은 필수이고 그 외 List/Detail 구성은 자유롭게 구성하시면 됩니다.
    - List 화면에 이름 표기 / Detail 화면에 이름, 이미지, 국가, 이메일, 번호 표기
- List는 Infinite Scroll 기능을 포함합니다.
    - 기능 추가
- Pull To Refresh 기능이 구현되어야 합니다.
    - 기능 추가
- 중복된 데이터는 제거 합니다.
    - loadData 에서 중복 제거 처리
- 자신 있는 아키텍처로 구성해주세요.
    - MVC 패턴 사용
