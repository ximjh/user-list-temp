//
//  UserDetailViewController.swift
//  userlist
//
//  Created by ximAir on 2023/05/12.
//

import Foundation
import UIKit

class UserDetailViewController: UIViewController {
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgUserImg: UIImageView!
    @IBOutlet weak var lblUserEmail: UILabel!
    @IBOutlet weak var lblUserPhone: UILabel!
    @IBOutlet weak var lblCountry: UILabel!
    
    var userDetailModel: UserModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if userDetailModel == nil {
            print("not exsit user detail info")
            self.dismiss(animated: false)
        }
        
        lblUserName.text = userDetailModel?.name
        lblUserEmail.text = userDetailModel?.email
        lblUserPhone.text = userDetailModel?.phone
        lblCountry.text = userDetailModel?.country
        
        loadUserImage()
        
    }
    
    func loadUserImage() {
        guard let imgUrl = URL(string: userDetailModel?.thumbnail ?? "" ) else { return }
        
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: imgUrl) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.imgUserImg.image = image
                    }
                }
            }
        }
    }
    
}
