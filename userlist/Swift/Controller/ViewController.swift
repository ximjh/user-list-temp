//
//  ViewController.swift
//  userlist
//
//  Created by ximAir on 2023/05/10.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // View Object
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var genderGroup: UISegmentedControl!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    // api using param
    var page: Int = 1
    var selectGender: String = AppConfig.FEMALE
    let resultCount: Int = 24
    let inc: String = "name,gender,email,phone,picture,location"
    
    // userModel List
    var userArray: [UserModel] = []
    
    // flag
    var endFlag: Bool = false
    var refreshFlag: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // tableView init
        tableView.dataSource = self
        tableView.delegate = self
        // tableView refreshControl init
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.isHidden = true
        tableView.refreshControl?.addTarget(self, action: #selector(pullRefreshHandler(_:)), for: .valueChanged)
        
        // indicator init
        indicator.hidesWhenStopped = true
        
        // init load user list
        loadUserData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("viewWillAppear")
        self.navigationController?.isNavigationBarHidden = true
    }
    
    // segmentControl handler
    @IBAction func genderGroupChangeHandler(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            print("female")
            selectGender = AppConfig.FEMALE
        } else {
            print("male")
            selectGender = AppConfig.MALE
        }
        
        userArray = []
        
        loadUserData()
    }
    
    // api call & load user list
    func loadUserData() {
        if !refreshFlag {
            self.indicator.startAnimating()
        }
        
        let param: Parameters = [
            "page": page,
            "results": resultCount,
            "inc": inc,
            "gender": selectGender
        ]
        
        ApiService.getMethod(url: AppConfig.API_URL, parameter: param, completionHandler: { res in
            switch res.result {
            case .success(let data):
                let jsonObject = JSON(data)
                let result = jsonObject["results"]
                print("result: \(result)")
                
                if !result.isEmpty {
                    for model in result {
                        let modelPackge = model.1
                        let namePackge = modelPackge["name"]
                        let name = namePackge["title"].stringValue + " " + namePackge["last"].stringValue + " " + namePackge["first"].stringValue
                        
                        // 중복 제거
                        if !self.userArray.isEmpty {
                            self.userArray.removeAll(where: { $0.name == name })
                        }
                        
                        let picturePackge = modelPackge["picture"]
                        
                        let locationPackge = modelPackge["location"]
                        
                        let userModel = UserModel(name: name, email: modelPackge["email"].stringValue, phone: modelPackge["phone"].stringValue, picture: picturePackge["large"].stringValue, gender: modelPackge["gender"].stringValue, thumbnail: picturePackge["large"].stringValue, country: locationPackge["country"].stringValue)
                        
                        self.userArray.append(userModel)
                    }
                    
                    self.tableView.reloadData()
                    
                    self.indicator.stopAnimating()
                    
                    self.endFlag = false
                    self.refreshFlag = false
                }
                
                break
            case .failure(let err):
                print("api error : \(err)")
                break
            default:
                print("api default case")
                break
            }
        })
    }

    // table row count
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userArray.count
    }
    
    // table row create
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        if !userArray.isEmpty {
            let text: String = userArray[indexPath.row].name
            cell.textLabel?.text = text
        }
        return cell
    }
    
    // table row click
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let userDetailView = self.storyboard?.instantiateViewController(withIdentifier: "UserDetailViewController") as! UserDetailViewController
        userDetailView.userDetailModel = userArray[indexPath.row]
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.pushViewController(userDetailView, animated: true)
    }
    
    // table scroll event
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.height {
            if !endFlag {
                endFlag = true
                loadUserData()
            }
        }
    }
    
    // table refresh event
    @objc func pullRefreshHandler(_ sender: Any) {
        refreshFlag = true
        
        DispatchQueue.main.async {
            self.userArray = []
            self.loadUserData()
            
            self.tableView.refreshControl?.endRefreshing()
        }
    }
}

