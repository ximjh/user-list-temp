//
//  AppConfig.swift
//  userlist
//
//  Created by ximAir on 2023/05/10.
//

import Foundation

public struct AppConfig {
    public static let API_URL = "https://randomuser.me/api/"
    public static let MALE = "male"
    public static let FEMALE = "female"
}
