//
//  UserModel.swift
//  userlist
//
//  Created by ximAir on 2023/05/10.
//

import Foundation

struct UserModel {
    let name: String
    let email: String
    let phone: String
    let picture: String
    let gender: String
    let thumbnail: String
    let country: String
}
