//
//  ApiService.swift
//  userlist
//
//  Created by ximAir on 2023/05/10.
//

import Foundation
import Alamofire

class ApiService {
    static let shared: ApiService = ApiService()
    
    class func getMethod(url: String, parameter: Parameters, completionHandler: @escaping (AFDataResponse<Data>) -> Void) {
        AF.request(url, method: .get, parameters: parameter).responseData(completionHandler: completionHandler)
    }

}
